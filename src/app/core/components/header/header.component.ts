import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { SettingsComponent } from '../../../shared/components/settings/settings.component';
import { Owner } from '../../models/owner.model';
import { ModalService } from '../../services/modal.service';
import { UserService } from '../../services/user.service';
import { environment } from '../../../../environments/environment';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
	@Input() public appTitle = environment.appTitle;
	@Input() public appLogo = environment.appLogo;
	@Input() public linkToHome: boolean = true;
	mobileMenuVisible = false;
	public environment = environment;

	constructor() {}

	public ngOnInit(): void {}
}
