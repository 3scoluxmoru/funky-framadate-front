/**
 * liste des fichiers JSON de traduction des textes dans l'appli à charger par ngx-translate et prposer aux utilisateurs et utilisatrices de Framdate.
 * Cette enum est utilisé dans le composant de sélection de langue.
 */
export enum LanguageEnum {
	ar = 'ar',
	br = 'br',
	ca = 'ca',
	de = 'de',
	en = 'en',
	eo = 'eo',
	es = 'es',
	fr = 'fr',
	gl = 'gl',
	hu = 'hu',
	it = 'it',
	ja = 'ja',
	nl = 'nl',
	oc = 'oc',
	sv = 'sv',
}
