import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class LoaderService {
	private _loadingStatus: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
	public readonly isLoading: Observable<boolean> = this._loadingStatus.asObservable();

	public setStatus(status: boolean): void {
		this._loadingStatus.next(status);
	}
}
