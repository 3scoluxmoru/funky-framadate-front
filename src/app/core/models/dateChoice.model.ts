export interface DateChoice {
	literal: string;
	timeSlices: TimeSlices[];
	date_object: Date;
	date_input: string;
}

export interface TimeSlices {
	literal: string;
}

export const timeSlicesProposals: TimeSlices[] = [
	{ literal: 'matin' },
	// { literal: 'midi' },
	// { literal: 'après-midi' },
	// { literal: 'soir' },
	// { literal: 'aux aurores' },
	// { literal: 'au petit dej' },
	// { literal: 'au deuxième petit dej des hobbits' },
];
