import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdministrationComponent } from './administration.component';
import { StepTwoComponent } from './form/steps/step-two/step-two.component';
import { StepThreeComponent } from './form/steps/step-three/step-three.component';
import { StepFourComponent } from './form/steps/step-four/step-four.component';
import { StepFiveComponent } from './form/steps/step-five/step-five.component';
import { StepOneComponent } from './form/steps/step-one/step-one.component';
import { SuccessComponent } from './success/success.component';
import { AdminConsultationComponent } from './consultation/consultation.component';
import { StepSixComponent } from './form/steps/step-six/step-six.component';
import { StepSevenComponent } from './form/steps/step-seven/step-seven.component';

const routes: Routes = [
	{
		path: '',
		component: AdministrationComponent,
	},
	{ path: 'key/:admin_key', component: AdminConsultationComponent },
	{
		path: 'step',
		children: [
			{ path: '1', component: StepOneComponent },
			{ path: '2', component: StepTwoComponent },
			{ path: '3', component: StepThreeComponent },
			{ path: '4', component: StepFourComponent },
			{ path: '5', component: StepFiveComponent },
			{ path: '6', component: StepSixComponent },
			{ path: '7', component: StepSevenComponent },
		],
	},
	{
		path: 'success',
		component: SuccessComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class AdministrationRoutingModule {}
