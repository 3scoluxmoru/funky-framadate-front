import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
	selector: 'app-text-select',
	templateUrl: './text-select.component.html',
	styleUrls: ['./text-select.component.scss'],
})
export class TextSelectComponent implements OnInit {
	@Input()
	public form: UntypedFormGroup;
	public choices = [];

	constructor() {}

	ngOnInit(): void {}
	reinitChoices(): void {}
	addChoice(): void {}
	deleteChoiceField(i): void {}
	keyOnChoice($event, i): void {}
}
